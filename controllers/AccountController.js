
const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

var alasql = require('alasql');


const mlabBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8edsip/collections/';
//const mlabAPIKey='apiKey=_c09Zj-xlJNtJSpnn_m7GLbzvKGwBOPo';

const mlabAPIKey='apiKey='+process.env.MLAB_API_KEY;


function getAccountsbyuserid(req,res) {
  console.log("GET /apitechu/v2/accounts/:id");
  var id  = req.params.id;

  var query = 'q={"userid":' + id + '}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("eated");
  httpClient.get("account?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo las cuentas"}

      var response1 = alasql('SELECT IBAN, SUM(CAST(balance as NUMBER)) AS balance,count(*) AS MOVS FROM ? GROUP BY IBAN',[response]);

      console.log(response1);
      res.send(response1);
    });

}




  function deleteAccountV2 (req, res) {
        console.log("DELETE /apitechu/accounts/");
        console.log("LaIBAN enviada es " + req.body.IBAN);



        var query = 'q={"IBAN":"' + req.body.IBAN + '"}';
        var httpClient = requestJson.createClient(mlabBaseURL);
        console.log("IBAN finding");
        httpClient.get("account?" + query + '&'+ mlabAPIKey,
          function(err,resMLab,body){
            console.log(err );
            var response = !err ?
              body : {"msg" : "Error obteniendo las cuentas"}

            console.log(response);
            for (var i=0; i<response.length; i++) {
              console.log("una");
              console.log(response[i]['_id']['$oid']);
              var id_borrar = response[i]['_id']['$oid'];
              var  httpClient = requestJson.createClient(mlabBaseURL); // requestJson es una librería externa que hemos importado
              console.log("Client created");
              var deleted = false;

              httpClient.delete("account/" +id_borrar +'?'+ mlabAPIKey ,
                 function(err, resMLab, body) {
                   console.log("Cuenta eliminada con éxito");
                   console.log(body);
                   console.log(err);
                   // console.log(resMLab);
                   res.status(200); // Created

                 });

             }
           });

          console.log("sale");
          res.send({"msg" : "Cuenta eliminada   con éxito"});
      }

function getUsersV2(req, res) {

  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client created");
  httpClient.get("user?" + mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}
      res.send(response);
    });

}


function getUserByIdV2(req,res) {
  console.log("GET /apitechu/v2/users/:id");
  var id  = req.params.id;
  var query = 'q={"id":' + id + '}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("eated");
  httpClient.get("user?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}
      res.send(response);
    });

}

function createTX (req,res) {
   console.log("POST /apitechu/account/");
   console.log(req.body.IBAN);
   console.log(req.body.userid);
   console.log(req.body.balance);
   var newTX = {
                     "IBAN" : req.body.IBAN,
                     "userid" : req.body.userid,
                     "balance" : req.body.balance
             };
    var  httpClient = requestJson.createClient(mlabBaseURL); // requestJson es una librería externa que hemos importado
    console.log("TX created");
    httpClient.post("account?" + mlabAPIKey, newTX,
                function(err, resMLab, body) {
                  console.log("transaccion guardado con éxito");
                  res.status(200); // Created
                  res.send({"msg" : "transaccion guardado con éxito"});
             }
        )
    }





function deleteTX(req,res) {
   console.log("DELETE /apitechu/accounts/:id");
   console.log(req.params.id)
   var users = require('../usuarios.json');
   	for (var i=0 ; i< users.length; i++) {
   		console.log(users[i].id);
   		if ( users[i].id == req.params.id) {
   			console.log ("EStamos en el a borrar");
   			if (i > -1) {
     				users.splice(i, 1);
   			}
   		}
   	}

   console.log(users);
   res.send("msgHola desde API TechU");
 }



module.exports.getUsersV2 = getUsersV2;

module.exports.getUserByIdV2 = getUserByIdV2;

module.exports.createTX = createTX;

module.exports.getAccountsbyuserid = getAccountsbyuserid;

module.exports.deleteAccountV2 = deleteAccountV2;
