

const io = require('../io');

const requestJson = require('request-json');

const mlabBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8edsip/collections/';
//const mlabAPIKey='apiKey=_c09Zj-xlJNtJSpnn_m7GLbzvKGwBOPo';

const mlabAPIKey='apiKey='+process.env.MLAB_API_KEY;



function loginUserV1(req,res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body);
  var datos_entrada = req.body;

  console.log(datos_entrada.email);
  console.log(datos_entrada.password);
  var users = require("../usuariosv2.json");



  var indexofemail = users.findIndex (
        function (element) {
            return element.email == datos_entrada.email;
          }
  );
  console.log(indexofemail);
  if (indexofemail == -1) {
    res.send ("NOK");
    }
  else {
    console.log (users[indexofemail].password)
    if (users[indexofemail].password == datos_entrada.password) {
      res.send ("OK");
    }
    else {
      res.send ("NOK del password");
      }

    }

}


function is_logged(req,res) {
  console.log("GET /apitechu/is_logged/:id");
  var id  = req.params.id;

  console.log ("el id a buscar " + id )

  var query = 'q={"id":"' + id + '"}';

  console.log (query);
  var salida;

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client query");
  httpClient.get("user?" + query + '&'+ mlabAPIKey,
  function(err,resMLab,body){
    //console.log(body);
    var response = !err ?
    body : {"msg" : "Error obteniendo los usuarios"}

    var salida = body;

    if (body.length == 0) {
      console.log("no existe el usuario");
      response = {"msg" : "usuario not loggead"}


      res.send(response);
    }
    else {
      response = {"msg" : "usuario loggead"}


      res.send(response);

    }
  });


}



function loginUserV2(req,res) {
  console.log("POST /apitechu/v2/login");

  console.log(req.body);
  var datos_entrada = req.body;

  console.log(datos_entrada.email);
  console.log(datos_entrada.password);

  var query = 'q={"email":"' + datos_entrada.email + '"}';

  console.log (query);
  var salida;

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client query");
  httpClient.get("user?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      //console.log(body);
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}

      var salida = body;

      if (body.length == 0) {
        console.log("no existe el usuario");
        console.log(body);
        var response = {
         "mensaje" : "Login incorrecto, usuario no encontrado"
        }
        res.send(response);
      } else {
        console.log(" existe el usuario");
        console.log(body[0].email);
        console.log(body[0].password);
        if (body[0].password == datos_entrada.password) {
          console.log ("es el mismo password");
          var putBody =  '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mlabAPIKey,JSON.parse(putBody) , function(err, res,body) {
            var response = !err ?
              body : {"msg": "no he podido poner logged"};

            console.log(response);
          });
          var response = {
             "msg" : "Usuario logado con éxito",
             "userid" : body[0].id,
             "first_name" : body[0].first_name,
             "last_name": body[0].last_name,
             "logged": true,
             "email": body[0].email
           }
           res.send(response);


        } else {
          var response = {
            "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
            }
          res.send(response);
          console.log ("No es el mismo password");
        }

      }

    });

    console.log(salida);

/*  console.log(indexofemail);
  if (indexofemail == -1) {
    res.send ("NOK");
    }
  else {
    console.log (users[indexofemail].password)
    if (users[indexofemail].password == datos_entrada.password) {
      res.send ("OK");
    }
    else {
      res.send ("NOK del password");
      }

    }*/
    //res.send("ya ta");

}



function logoutUserV2(req,res) {
  console.log("post /apitechu/v2/logout");
  console.log(req.body)
  console.log("Body");
  var datos_entrada = req.body;

  console.log(datos_entrada.email);
  console.log(datos_entrada.password);

  var query = 'q={"email":"' + datos_entrada.email + '"}';

  console.log (query);
  var salida;

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client query");
  httpClient.get("user?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      //console.log(body);
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}

      var salida = body;

      if (body.length == 0) {
        console.log("no existe el usuario");
        console.log(body);
        var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
        }
        res.send(response);
      } else {
        console.log(" existe el usuario");
        console.log(body[0].email);
        console.log(body[0].password);
        console.log ("es el mismo password");
        var putBody =  '{"$unset":{"logged":""}}';
        httpClient.put("user?" + query + "&" + mlabAPIKey,JSON.parse(putBody) , function(err, res,body) {
            var response = !err ?
              body : {"msg": "no he podido poner logged"};

            console.log(response);

          });

        var response = {
         "mensaje" : "Logout correcto"
        }
        res.send(response);


      }



    });




}


function logoutUserV1(req,res) {
  console.log("post /apitechu/v1/logout");
  console.log("Body");
  var users = require("../usuariosv2.json");
  console.log(req.body) ;
  console.log(req.body.email) ;
  console.log(req.body.password) ;
  var logged = false;
  var index = users.findIndex(
    users => users.id == req.body.id  && users.logged == true
  );
  if (index > 0) {
          console.log("User found !!!");
      res.send('{"mensaje" : "logout correcto", "idUsuario" :"+users[index].id+"}');
      users[index].logged = false;

      io.writeUserDataToFile(users);
   } else {
     res.send('{"mensaje" : "logout incorrecto"}');
   }
}





module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;


module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;

module.exports.is_logged = is_logged;
