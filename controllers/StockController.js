
const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8edsip/collections/';
//const mlabAPIKey='apiKey=_c09Zj-xlJNtJSpnn_m7GLbzvKGwBOPo';

const mlabAPIKey='apiKey='+process.env.MLAB_API_KEY;


function getStocksbyuserid(req,res) {
  console.log("GET /apitechu/stocks");
  var id  = req.params.id;
  console.log(req.params.id);

  var query = 'q={"userid":' + id + '}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("eated");
  httpClient.get("stocks?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo las cuentas"}
      res.send(response);
    });

}



function getUsersV2(req, res) {

  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client created");
  httpClient.get("user?" + mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}
      res.send(response);
    });

}


function getUserByIdV2(req,res) {
  console.log("GET /apitechu/v2/users/:id");
  var id  = req.params.id;
  var query = 'q={"id":' + id + '}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("eated");
  httpClient.get("user?" + query + '&'+ mlabAPIKey,
    function(err,resMLab,body){
      console.log(err );
      var response = !err ?
        body : {"msg" : "Error obteniendo los usuarios"}
      res.send(response);
    });

}

function createUserV2 (req,res) {
   console.log("POST /apitechu/v2/users");
   console.log(req.body.id);
   console.log(req.body.first_name);
   console.log(req.body.last_name);
   console.log(req.body.email);
   var newUser = {
                     "id" : req.body.id,
                     "first_name" : req.body.first_name,
                     "last_name" : req.body.last_name,
                     "email" : req.body.email,
                     "password" : crypt.hash(req.body.password)
             };
    var  httpClient = requestJson.createClient(mlabBaseURL); // requestJson es una librería externa que hemos importado
    console.log("Client created");
    httpClient.post("user?" + mlabAPIKey, newUser,
                function(err, resMLab, body) {
                  console.log("Usuario guardado con éxito");
                  res.status(200); // Created
                  res.send({"msg" : "Usuario guardado con éxito"});
             }
        )
    }


function createUserV1 (req,res) {
     console.log("POST /apitechu/v1/users");
     res.send('{"msg”:"Hola desde API TechU"}')
     console.log(req.body);
     console.log(req.body.first_name)
     var users = require('../usuarios.json');
     var count = users.length
     console.log(count);
     var newUser = {
        "id" : count+1,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
     }
     users.push(newUser);
     io.writeUserDataToFile(users);
     console.log("Usuario creado con exito");
     count = users.length;
     console.log(count);
     res.send("Usuario creado con exito")
   }

function  getUsersV1(req,res) {
        console.log("GET /apitechu/v1/users");
	//res.sendFile('usuarios.json', {root: __dirname});
	var users = require('../usuarios.json');
	var count = users.length
	var salida = {};

		//validando cada uno de los valores que se envian

	if (req.query.$count == 'true') {
		salida['count'] = count;
		}
	if (req.query.$top) {
		var top = users.slice(0,req.query.$top)
		salida['top'] = top;
	}
	console.log(req.query.length)
	if (Object.keys(req.query).length === 0) {
		salida['top'] = users;
        }
	res.send(salida);
	}


function deleteUserV1(req,res) {
   console.log("DELETE /apitechu/v1/users/:id");
   console.log(req.params.id)
   var users = require('../usuarios.json');
   	for (var i=0 ; i< users.length; i++) {
   		console.log(users[i].id);
   		if ( users[i].id == req.params.id) {
   			console.log ("EStamos en el a borrar");
   			if (i > -1) {
     				users.splice(i, 1);
   			}
   		}
   	}


   //	for (x in users) {
   //		console.log(users[x].id);
   //		if ( users[x].id == req.params.id) {
   //			users.splice(x, 1);
   //		}
   //	}



   //	for (const y  of users) {
   //		console.log("flasdfjs" + y.id);
   //		if ( y.id == req.params.id) {
   //			delete users[users.indexOf(y)];
   //                      //users.splice(y, 1);
   //               }
   //	}


   //	var indice = users.findIndex( function encontrar(datos){
   //        console.log("el id es " + req.params.id);
   //		return  datos.id == req.params.id;
   //        }
   //	);

   //	user.splice(indice,1);


   //console.log("entra " + datos);


   //});


   console.log(users);
   res.send("msgHola desde API TechU");
 }



// module.exports.createUserV1 = createUserV1;
// module.exports.deleteUserV1 = deleteUserV1;
// module.exports.getUsersV1 = getUsersV1;
// module.exports.getUsersV2 = getUsersV2;
//
// module.exports.getUserByIdV2 = getUserByIdV2;
//
// module.exports.createUserV2 = createUserV2;

module.exports.getStocksbyuserid = getStocksbyuserid;
