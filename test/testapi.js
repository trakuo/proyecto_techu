const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../index');

describe('First test',
  function() {
    it('Test that Duckduckgo works', function(done) {
      chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function(err, res) {
            console.log("Request finished");
            //console.log(err);
            //console.log(res);

            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)

describe('Test de API de usuarios',
  function() {
    it('Prueba que la API de usaurios responde', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users/hello/')
        .end(
          function(err, res) {
            console.log("Request finished");
            console.log("priimer test")
            console.log(res);
            //res.should.have.status(200);
            //res.body.msg.should.be.eql("Todo OK");
            done();
          }
        )
      }
    )
  }
)

describe('Test de API de usuarios listado',
  function() {
    it('Prueba que la API de usaurios responde', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users/')
        .end(
          function(err, res) {
            console.log("Request finished");
            console.log(res);
            console.log("pasa con nada")

            //for (user of res) {
            //  console.log (user)
              //user.should.have.property("password");
              //user.should.have.property("email");
            //}
            console.log(res);
            //res.should.have.status(200);
            //res.body.msg.should.be.eql("Hola desde API TechU!");
            done();
          }
        )
      }
    )
  }
)
